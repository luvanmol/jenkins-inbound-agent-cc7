ARG version=4.9-1
FROM jenkins/inbound-agent:$version AS agent
FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/cc7-openjdk:jdk-8

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000

RUN groupadd -g ${gid} ${group}
RUN useradd -c "Jenkins user" -d /home/${user} -u ${uid} -g ${gid} -m ${user}

COPY --from=agent /usr/share/jenkins /usr/share/jenkins
COPY --from=agent /usr/local/bin/jenkins-agent /usr/local/bin/jenkins-agent
COPY --from=agent /usr/local/bin/jenkins-slave /usr/local/bin/jenkins-slave

USER jenkins

ENTRYPOINT ["/usr/local/bin/jenkins-agent"]
